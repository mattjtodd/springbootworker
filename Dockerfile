FROM frolvlad/alpine-oraclejdk8:slim

ADD ["target/spring-boot-worker-0.1.0-SNAPSHOT.jar", "app.jar"]
ADD ["http://central.maven.org/maven2/io/prometheus/jmx/jmx_prometheus_javaagent/0.8/jmx_prometheus_javaagent-0.8.jar", "tomcat.yml", "./"]

EXPOSE 8080

CMD [ "java", "-javaagent:/jmx_prometheus_javaagent-0.8.jar=7071:/tomcat.yml", "-Dmanagement.security.enabled=false", "-Dmanagement.port=8081", "-jar", "/app.jar" ]
