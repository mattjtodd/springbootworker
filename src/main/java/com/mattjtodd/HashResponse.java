package com.mattjtodd;

import java.util.Arrays;

public class HashResponse {
    private final byte[] hash;

    private final int iterations;

    private final String algorithm;

    private final int keyLength;

    private final int saltBytes;

    public HashResponse(byte[] hash, int iterations, String algorithm, int keyLength, int saltBytes) {
        this.hash = copyOf(hash);
        this.iterations = iterations;
        this.algorithm = algorithm;
        this.keyLength = keyLength;
        this.saltBytes = saltBytes;
    }

    public HashResponse(byte[] hash, HashRequest request) {
        this(hash, request.getIterations(), request.getAlgorithm(), request.getKeyLength(), request.getSaltBytes());
    }

    public byte[] getHash() {
        return copyOf(hash);
    }

    public int getIterations() {
        return iterations;
    }

    public String getAlgorithm() {
        return algorithm;
    }

    public int getKeyLength() {
        return keyLength;
    }

    public int getSaltBytes() {
        return saltBytes;
    }

    @Override
    public String toString() {
        return "HashRequest{" +
            "hash=" + Arrays.toString(hash) +
            ", iterations=" + iterations +
            ", algorithm='" + algorithm + '\'' +
            ", keyLength=" + keyLength +
            ", saltBytes=" + saltBytes +
            '}';
    }

    private static byte[] copyOf(byte[] array) {
        return Arrays.copyOf(array, array.length);
    }
}
