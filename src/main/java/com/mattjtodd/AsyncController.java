package com.mattjtodd;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

@RestController
class AsyncController {

    @Autowired
    private ScheduledExecutorServices executorServices;

    @RequestMapping(value = "/async")
    @ResponseBody
    DeferredResult<String> async(@RequestParam(defaultValue = "1000")  int taskDuration) {
        DeferredResult<String> result = new DeferredResult<>();
        task(executorServices, taskDuration)
            .thenAccept(result::setResult);
        return result;
    }

    @RequestMapping(value = "/sync")
    @ResponseBody
    String sync(@RequestParam(defaultValue = "1000") int taskDuration) {
        return task(executorServices, taskDuration).join();
    }

    private static CompletableFuture<String> task(ScheduledExecutorServices timer, int duration) {
        return timer.schedule(() -> "Done", duration, TimeUnit.MILLISECONDS);
    }
}
