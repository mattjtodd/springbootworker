package com.mattjtodd;

import javaslang.control.Try;

import javax.annotation.PreDestroy;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ScheduledExecutorServices {
    private final ScheduledExecutorService executorService;

    public ScheduledExecutorServices() {
        executorService = Executors.newScheduledThreadPool(10);
    }

    public <T> CompletableFuture<T> schedule(Supplier<T> task, int duration, TimeUnit unit) {
        CompletableFuture<T> future = new CompletableFuture<>();
        executorService.schedule(() -> future.complete(task.get()), duration, unit);
        return future;
    }

    @PreDestroy
    public void destroy() {
        Try
            .of(() -> executorService.awaitTermination(30, TimeUnit.SECONDS))
            .filter(Boolean.TRUE::equals)
            .onFailure(thrown ->
                Logger.getAnonymousLogger().log(Level.SEVERE, "Problem shutting down the Async processor", thrown));

        executorService.shutdown();
    }
}
