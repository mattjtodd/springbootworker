package com.mattjtodd;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.logging.Logger;

class HashService {

//    Map<String, List<String>> home(int iterations) throws Exception {
//
//        encode(new HashRequest("hashingtest".toCharArray(), iterations, "PBKDF2WithHmacSHA512", 256, 64), saltSeller);
//    }

    public static HashResponse encode(HashRequest params, SaltSeller randomSource) throws InvalidKeySpecException,
        NoSuchAlgorithmException {
        Logger.getAnonymousLogger().info("Encoding: " + params);
        int iterations = params.getIterations();
        byte[] salt = randomSource.salt(params.getSaltBytes());

        PBEKeySpec spec = new PBEKeySpec(params.getCharacters(), salt, iterations, params.getKeyLength());
        SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance(params.getAlgorithm());

        byte[] hash = secretKeyFactory.generateSecret(spec).getEncoded();

        return new HashResponse(hash, params);
    }
}
