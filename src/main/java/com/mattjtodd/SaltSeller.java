package com.mattjtodd;

interface SaltSeller {
    byte[] salt(int length);
}
